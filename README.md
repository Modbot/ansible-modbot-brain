Modbot Brain Dependency Ansible Role
=========

This role installs the dependencies required to run and develop with the Modbot Brain.

Installs
------------
- ROS
- Go 
- with apt-get:
  - ntp
  - udev
  - mercurial
  - autoconf
  - libtool
  - g++
  - cmake
  - python-pip
  - libncurses5-dev
  - libncursesw5-dev
- Conan
  - Conan
  - modbot Conan remote
  - bincrafters Conan Remote
- ROS
  - ROS desktop
  - ROS urdf
  - ROS ompl
  - ROS trac-ik

Requirements
------------
N/A

Role Variables
--------------
N/A

Dependencies
------------
- Modbot's forked version of JAlessio's ROS role.
- Modbot's forked version of JoshuaLund's Go role.
