# Ansible Modbot Brain
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [Unreleased]

### Removed

- Removed development dependencies. They have migrated to `ansible-development-dependencies`.


## [1.2.0] - 2018-05-22

### Fixed

- Source ROS version `setup.sh` is added to `~/.bashrc` if it is not already present.
- Pip is installed if is not already on the system.


## [1.1.0]

### Added

- development-pip.yml installs cpplint and watchdog via pip
- ubuntu.yml becomes development-ubuntu.yml
- the development-*.yml tasks install with the conditional `install-development-dependencies`.


## [1.0.0]

### Added

- main.yml
  - conan.yml
  - ubuntu.yml
  - ros.yml
- conan.yml
  - install conan
  - add conan remotes; modbot, bincrafters
- ros.yml
  - installs additional ros packages
- ubuntu.yml
  - updates apt-get, installs apt packages.